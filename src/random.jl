

for Rot ∈ (:RotX, :RotY, :RotZ)
    @eval function Base.rand(rng::AbstractRNG, ::Type{<:$Rot{T}}) where {T}
        $Rot{T}(2π*rand(rng))
    end
    @eval Base.rand(rng::AbstractRNG, ::Type{<:$Rot}) = rand(rng, $Rot{Float64})
end

# these are pretty dubious because of stretching of Float64 but seem fair to define
for Boost ∈ (:BoostX, :BoostY, :BoostZ)
    @eval function Base.rand(rng::AbstractRNG, ::Type{<:$Boost{T}}) where {T}
        $Boost{T}(rapidity(rand(rng)))
    end
    @eval Base.rand(rng::AbstractRNG, ::Type{<:$Boost}) = rand(rng, $Boost{Float64})
end
