
"""
    LorIdentity{T} <: Lorentz{T}

The identity element of the Lorentz group, as a matrix containing no run-time data.  Most operations
with this object are more efficient than the equivalent with a materialized `SMatrix{4,4}`.
The purpose of this object is primarily as a placeholder in generic code.
"""
struct LorIdentity{T} <: Lorentz{T} end

LorIdentity() = LorIdentity{Float64}()

function Base.getindex(I::LorIdentity{T}, k::Int) where {T}
    if k == 1
        one(T)
    elseif k ≤ 5
        zero(T)
    elseif k == 6
        one(T)
    elseif k ≤ 10
        zero(T)
    elseif k == 11
        one(T)
    elseif k ≤ 15
        zero(T)
    elseif k == 16
        one(T)
    else
        throw(BoundsError(I,k))
    end
end

Base.inv(I::LorIdentity) = I

Base.:(*)(::LorIdentity, v::StaticVector{4}) = v
Base.:(*)(::LorIdentity, Λ::Lorentz) = Λ
Base.:(*)(Λ::Lorentz, ::LorIdentity) = Λ

Base.one(::Type{Lorentz{T}}) where {T} = LorIdentity{T}()
Base.one(::Type{Lorentz}) = LorIdentity{Float64}()


"""
    fourvelfromcoordvel(u)

Compute a properly normalized velocity vector from the coordinate velocity ``u``,
given as a 3-vector.  This gives a correct result for tachyons but will diverge
(result can contain `Inf` or `NaN`) for null vectors.
"""
function fourvelfromcoordvel(u::StaticVector{3,T}) where {T}
    γ = lorentzfactor(norm(u))
    v = @SVector T[u[1], u[2], u[3], one(T)]
    γ*v
end
fourvelfromcoordvel(u::AbstractVector) = fourvelfromcoordvel(convert(SVector{3}, u))


"""
    comoving(u; normalize=true)

Creates the boost into the comoving frame of ``u`` as a [`LorMatrix`](@ref).  This is given by
```math
{\\Lambda^\\mu}_\\nu=\\left(\\begin{array}{cc}
    {\\delta^i}_j + \\frac{u^i u_j}{u_4 + 1} & -u_j \\\\
    -u^i & u_4
\\end{array}\\right)
```

If `normalize=true`, ``u`` will be normalized.  If `normalize=false`, no check will be performed so that
the result is undefined if ``u`` is not normalized.
"""
function comoving(u::StaticVector{4,T}; normalize::Bool=true) where {T}
    normalize && (u = minkowskinormalize(u))
    A = u[4] + one(T)
    o = @SMatrix T[one(T)+u[1]^2/A  u[1]*u[2]/A  u[1]*u[3]/A  -u[1]
                   u[2]*u[1]/A  one(T)+u[2]^2/A  u[2]*u[3]/A  -u[2]
                   u[3]*u[1]/A  u[3]*u[2]/A  one(T)+u[3]^2/A  -u[3]
                   -u[1]  -u[2]  -u[3]  u[4]
                  ]
    LorMatrix(o)
end
comoving(u::AbstractVector; kw...) = comoving(convert(SVector{4}, u); kw...)
