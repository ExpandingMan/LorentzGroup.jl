
"""
    Generator{T<:Number} <: StaticMatrix{4,4,T}

Abstract type for generators of the 4-dimensional vector representation of the Lorentz group.
The generators form a vector space, so that addition between generators and multiplication of a
generator by a scalar returns a `Generator`.  Multiplication between generators, however, returns
a non-`Generator` `StaticMatrix`.
"""
abstract type Generator{T<:Number} <: StaticMatrix{4,4,T} end

StaticArrays.Size(::Type{<:Generator}) = Size(4,4)

"""
    GenMatrix{T} <: Generator{T}

A 4×4 `StaticMatrix` wrapper type for elements of the 4-dimensional vector representation of
the Lorentz algebra.  This type is analogous to [`LorMatrix`](@ref) and should be considered a
"fallback" for cases where elements of the algebra can't be represented more succinctly as with
e.g. [`Gen`](@ref) or [`ScaledGen`](@ref).

**WARN** Constructors for this type check the dimensionality of arguments but do *NOT* check
that the argument is a valid element of the representation.
"""
struct GenMatrix{T} <: Generator{T}
    data::SMatrix{4,4,T,16}

    GenMatrix{T}(tpl::NTuple{16}) where {T} = new{T}(SMatrix{4,4,T,16}(tpl))
    GenMatrix{T}(J::StaticMatrix{4,4}) where {T} = new{T}(Tuple(J))
end

GenMatrix(J::StaticMatrix{4,4,T}) where {T} = GenMatrix{T}(J)

Base.getindex(J::GenMatrix, k::Int) = getindex(J.data, k)

Base.:(+)(J₁::GenMatrix, J₂::GenMatrix) = GenMatrix(J₁.data + J₂.data)
Base.:(-)(J::GenMatrix) = GenMatrix(-J.data)
Base.:(-)(J₁::GenMatrix, J₂::GenMatrix) = GenMatrix(J₁.data - J₂.data)

gentype(J::Generator) = gentype(typeof(J))


"""
    Gen{T} <: Generator{T}

Abstract type for specific generators of the Lorentz group.  Each of the subtypes of `Gen`
is a generator in the following basis:
- `Gen23` (`RX`, x-rotation)
- `Gen14` (`BX`, x-boost)
- `Gen31` (`RY`, y-rotation)
- `Gen24` (`BY`, y-boost)
- `Gen12` (`RZ`, z-rotation)
- `Gen34` (`BZ`, z-boost)

We adopt a sign convention for the generators that simplifies their exponentiation, i.e. reduces
the need to worry about signs.  For each of the subtypes of `Gen`, the corresponding exponential
`exp(J)` gives the boost or rotation that everybody knows and loves (i.e. counterclockwise rotation,
forward boost).

As with `ScaledGen`, exponentiation of `Gen` returns an efficient [`Exp`](@ref) object.
In other words, the subtypes of `Gen` represent the basis in which group elements are maximally
efficient.
"""
abstract type Gen{T} <: Generator{T} end

# which spatial axis we associate generator with, usefor for basis vectors and stuff
axis(J::Gen) = axis(typeof(J))

struct Gen23{T} <: Gen{T} end
const RX = Gen23
Gen23() = Gen23{Float64}()
gentype(::Type{<:Gen23}) = Gen23
axis(::Type{<:Gen23}) = 1

struct Gen14{T} <: Gen{T} end
const BX = Gen14
Gen14() = Gen14{Float64}()
gentype(::Type{<:Gen14}) = Gen14
axis(::Type{<:Gen14}) = 1

struct Gen31{T} <: Gen{T} end
const RY = Gen31
Gen31() = Gen31{Float64}()
gentype(::Type{<:Gen31}) = Gen31
axis(::Type{<:Gen31}) = 2

struct Gen24{T} <: Gen{T} end
const BY = Gen24
Gen24() = Gen24{Float64}()
gentype(::Type{<:Gen24}) = Gen24
axis(::Type{<:Gen24}) = 2

struct Gen12{T} <: Gen{T} end
const RZ = Gen12
Gen12() = Gen12{Float64}()
gentype(::Type{<:Gen12}) = Gen12
axis(::Type{<:Gen12}) = 3

struct Gen34{T} <: Gen{T} end
const BZ = Gen34
Gen34() = Gen34{Float64}()
gentype(::Type{<:Gen34}) = Gen34
axis(::Type{<:Gen34}) = 3

Gen(J::Gen) = J

param(J::Gen) = one(eltype(J))


function Base.getindex(J::Gen23{T}, k::Int) where {T}
    if k ≤ 6
        zero(T)
    elseif k == 7
        one(T)
    elseif k ≤ 9
        zero(T)
    elseif k == 10
        -one(T)
    elseif k ≤ 16
        zero(T)
    else
        throw(BoundsError(J,k))
    end
end

function Base.getindex(J::Gen31{T}, k::Int) where {T}
    if k ≤ 2
        zero(T)
    elseif k == 3
        -one(T)
    elseif k ≤ 8
        zero(T)
    elseif k == 9
        one(T)
    elseif k ≤ 16
        zero(T)
    else
        throw(BoundsError(J,k))
    end
end

function Base.getindex(J::Gen12{T}, k::Int) where {T}
    if k == 1
        zero(T)
    elseif k == 2
        one(T)
    elseif k ≤ 4
        zero(T)
    elseif k == 5
        -one(T)
    elseif k ≤ 16
        zero(T)
    else
        throw(BoundsError(J,k))
    end
end

function Base.getindex(K::Gen14{T}, k::Int) where {T}
    if k ≤ 3
        zero(T)
    elseif k == 4
        -one(T)
    elseif k ≤ 12
        zero(T)
    elseif k == 13
        -one(T)
    elseif k ≤ 16
        zero(T)
    else
        throw(BoundsError(K,k))
    end
end

function Base.getindex(K::Gen24{T}, k::Int) where {T}
    if k ≤ 7
        zero(T)
    elseif k == 8
        -one(T)
    elseif k ≤ 13
        zero(T)
    elseif k == 14
        -one(T)
    elseif k ≤ 16
        zero(T)
    else
        throw(BoundsError(K,k))
    end
end

function Base.getindex(K::Gen34{T}, k::Int) where {T}
    if k ≤ 11
        zero(T)
    elseif k == 12
        -one(T)
    elseif k ≤ 14
        zero(T)
    elseif k == 15
        -one(T)
    elseif k == 16
        zero(T)
    else
        throw(BoundsError(K,k))
    end
end


"""
    ScaledGen{T,G<:Gen} <: Generator{T}

A scalar multiple of the generator `G`.  As such, `ScaledGen` are in the same basis as [`Gen`](@ref), and their
second type parameter describes the generator they are multiples of.

`ScaledGen` can be efficiently exponentiated in this basis, see [`Exp`](@ref).
"""
struct ScaledGen{T,G<:Gen} <: Generator{T}
    ϕ::T

    ScaledGen{T,G}(ϕ::Number) where {T<:Number,G<:Gen} = new{T,G}(convert(T, ϕ))
end

ScaledGen(::Type{G}, ϕ::Number=1.0) where {G<:Gen} = ScaledGen{typeof(ϕ),gentype(G)}(ϕ)
ScaledGen(J::Gen{T}, ϕ::Number=one(T)) where {T} = ScaledGen(typeof(J), ϕ)
ScaledGen{T1}(J::ScaledGen{T2,G}) where {T1,T2,G} = ScaledGen{T1,G}(J.ϕ)
ScaledGen(J::ScaledGen) = J

gentype(::Type{ScaledGen{T,G}}) where {T,G} = gentype(G)

Base.zero(::Type{ScaledGen{T,G}}) where {T,G} = ScaledGen{T,G}(zero(T))

function Base.:(*)(ϕ::Number, J::G) where {G<:Gen}
    S = promote_type(eltype(G), typeof(ϕ))
    ScaledGen(gentype(G){S}(), ϕ)
end
Base.:(*)(J::Gen, ϕ::Number) = ϕ*J

Base.:(*)(ϕ::Number, J::ScaledGen) = ScaledGen(gentype(J), ϕ*param(J))
Base.:(*)(J::ScaledGen, ϕ::Number) = ϕ*J

Base.getindex(J::ScaledGen{T,G}, k::Int) where {T,G} = J.ϕ*getindex(gentype(G){T}(), k)

param(J::ScaledGen) = J.ϕ

Base.:(-)(J::ScaledGen{T,G}) where {T,G} = ScaledGen{T,G}(-J.ϕ)
Base.:(-)(J::Gen) = ScaledGen(J, -one(eltype(J)))

function _plus(::Type{G}, ::Type{G}, J₁::Generator, J₂::Generator) where {G<:Gen}
    ϕ = param(J₁) + param(J₂)
    ScaledGen{typeof(ϕ),gentype(G)}(ϕ) 
end

_plus(::Type{<:Gen}, ::Type{<:Gen}, J₁::Generator, J₂::Generator) = GenMatrix(SMatrix(J₁) + SMatrix(J₂))

Base.:(+)(J₁::Generator, J₂::Generator) = _plus(gentype(J₁), gentype(J₂), J₁, J₂)

function _minus(::Type{G}, ::Type{G}, J₁::Generator, J₂::Generator) where {G<:Gen}
    ϕ = param(J₁) - param(J₂)
    ScaledGen{typeof(ϕ),gentype(G)}(ϕ)
end

_minus(::Type{<:Gen}, ::Type{<:Gen}, J₁::Generator, J₂::Generator) = GenMatrix(SMatrix(J₁) - SMatrix(J₂))

Base.:(-)(J₁::Generator, J₂::Generator) = _minus(gentype(J₁), gentype(J₂), J₁, J₂)

function _promote_eltypes(Js::NTuple{N,ScaledGen}) where {N}
    T = promote_type(ntuple(j -> eltype(Js[j]), N)...)
    (T, ntuple(j -> ScaledGen{T}(Js[j]), N))
end
