module LorentzGroup

using LinearAlgebra, Random
using StaticArrays


"""
    Lorentz{T<:Number} <: StaticMatrix{4,4,T}

Abstract type for 4-dimensional vector representations of the Lorentz group.
"""
abstract type Lorentz{T<:Number} <: StaticMatrix{4,4,T} end

StaticArrays.Size(::Type{<:Lorentz}) = Size(4,4)

#====================================================================================================
Notes on StaticArrays Interface:
- Multiplication wiht Base.AbstractVector is provided by StaticArrays
====================================================================================================#

"""
    N_GENERATED

The total number of factors of `Exp` for which efficient methods have been generated.  That is,
only `ExpMulti` with this man factors or less have efficient methods.
"""
const N_GENERATED = 3

"""
    lorentzfactor(v::Number)

Compute the Lorentz factor ``\\gamma`` for (coordinate) speed ``v``, defined as
```math
\\gamma^{-2} = |1 - v^2|
```
This does not error but gives a correct result for tachyonic velocities ``v > 1``,
which is relevant when normalizing spacelike vectors.
"""
lorentzfactor(v::T) where {T<:Number} = one(T)/√(abs(one(T) - v^2))

"""
    rapidity(v::Number)

Compute the rapidity parameter for (coordinate) speed ``v``.  Only valid for ``v < 1``.
"""
rapidity(v::Number) = sign(v)*acosh(lorentzfactor(v))


"""
    LorMatrix{T} <: Lorentz{T}

A 4×4 `StaticMatrix` wrapper type for elements of the vector representation of the Lorentz group.
This type should be considered a "fallback" for cases where group elements can't be represented more
succinctly in types such as [`Exp`](@ref).

**WARN** Constructors for this type check the dimensionality of arguments but do *NOT* check
that the argument is a valid element of the representation.
"""
struct LorMatrix{T} <: Lorentz{T}
    data::SMatrix{4,4,T,16}    

    # StaticArrays constructors are a huge pain; better to keep these inside
    LorMatrix{T}(tpl::NTuple{16}) where {T} = new{T}(SMatrix{4,4,T,16}(tpl))
    LorMatrix{T}(v::StaticMatrix{4,4}) where {T} = new{T}(Tuple(v))
end

LorMatrix(v::StaticMatrix{4,4,T}) where {T} = LorMatrix{T}(v)

Base.getindex(Λ::LorMatrix, j::Int) = getindex(Λ.data, j)

Base.:(*)(Λ₁::LorMatrix, Λ₂::LorMatrix) = LorMatrix(Λ₁.data * Λ₂.data)

# fallbacks
Base.:(*)(Λ₁::Lorentz, Λ₂::Lorentz) = LorMatrix(Λ₁)*LorMatrix(Λ₂)
Base.inv(Λ::Lorentz) = LorMatrix(inv(SMatrix(Λ)))


"""
    minkowski(u::AbstractVector, v::AbstractVector=u)

Compute ``\\eta_{\\mu\\nu}u^{\\mu}v^{\\nu}`` where ``\\eta`` is the Minkowski metric with signature ``(+,+,+,-)``.
"""
minkowski(u::StaticVector{4}, v::StaticVector{4}=u) = u[1]*v[1] + u[2]*v[2] + u[3]*v[3] - u[4]*v[4]
function minkowski(u::AbstractVector, v::AbstractVector=u)
    if length(u) ≠ 4 || length(v) ≠ 4
        throw(ArgumentError("invalid vector dimensions"))
    end
    u[1]*v[1] + u[2]*v[2] + u[3]*v[3] - u[4]*v[4]
end

"""
    minkowskinormalize(u::AbstractVector)

Normalizes the vector 4-vector ``u``.  This does *not* check if the vector is null, in which case the
result will diverge or contain `NaN`s.
"""
minkowskinormalize(u::AbstractVector) = u ./ √(abs(minkowski(u)))

η(u::AbstractVector, v::AbstractVector) = minkowski(u, v)


include("generators.jl")
include("exp.jl")

include("gen/exp_getindex.jl")
include("gen/exp_mult.jl")
include("gen/exp_alias.jl")

include("random.jl")
include("extras.jl")

include("precompile.jl")


export Lorentz
export lorentzfactor, rapidity, minkowski, minkowskinormalize
export LorIdentity, fourvelfromcoordvel, comoving
export LorMatrix
export GenMatrix
export RX, RY, RZ, BX, BY, BZ


end
