
"""
    Exp{T,G<:Gen} <: Lorentz{T}

An element of the 4-dimensional vector representation of the Lorentz group obtained by exponentiating one
of the generators [`ScaledGen`](@ref) (see also `Gen`).  That is, an [`Exp`](@ref) is a boost or rotation
in the standard basis.  In particular
```julia
RotX{T} = Exp{T,RX}
BoostX{T} = Exp{T,BX}
```
and likewise for the other axes.
"""
struct Exp{T,G<:Gen} <: Lorentz{T}
    generator::ScaledGen{T,G}

    Exp{T,G}(K::ScaledGen{T,G}) where {T,G} = new{T,G}(K)
end

"""
    gentype(Λ)

Returns the `Gen` type with which the generator or group element is associated.  For group
elements, this means it can be obtained via `exp(ϕ*J())` where `ϕ` is a real parameter and `J` is the generator
type.  This should be considered an implementation detail of the package and will error for group
elements that are not exponentials of one of the generators in the standard basis.
"""
gentype(Λ::Lorentz) = gentype(typeof(Λ))
gentype(::Type{Exp{T,G}}) where {T,G} = gentype(G)

Exp(J::ScaledGen{T,G}) where {T,G} = Exp{T,G}(J)
Exp(J::Gen) = Exp(ScaledGen(J))

Exp{T,G}(ϕ::Number) where {T,G} = Exp(ScaledGen{T,G}(ϕ))

# this looks really weird, but it should only get called when no params are supplied
(::Type{Exp{<:T,G}})(ϕ::Number) where {T,G} = Exp{typeof(ϕ),G}(ϕ)

params(Λ::Exp) = (Λ.generator.ϕ,)
param(Λ::Exp) = params(Λ)[1]

Base.exp(J::GenMatrix) = LorMatrix(exp(SMatrix(J)))
Base.exp(J::Gen) = Exp(J)
Base.exp(J::ScaledGen) = Exp(J)

_prod(::Type{G}, ::Type{G}, Λ₁::Exp, Λ₂::Exp) where {G<:Gen} = Exp(Λ₁.generator + Λ₂.generator)

Base.inv(Λ::Exp) = Exp(-Λ.generator)

Base.one(::Type{<:Exp{T,G}}) where {T,G} = Exp(zero(ScaledGen{T,G}))

# for testing
_slow_getindex(Λ::Exp, k::Int) = getindex(exp(SMatrix(Λ.generator)), k)


"""
    ExpMulti{N,T,Gs} <: Lorentz{T}

A type representing the product of [`Exp`](@ref) objects.  These are more general group elements that
cannot be obtained by exponentiating generators in the standard basis, but rather are obtained from
products of such exponentials.  For example
```julia
RotXY
BoostXRotYBoostZ
```
are aliases for `ExpMulti` objects.
"""
struct ExpMulti{N,T,Gs<:NTuple{N,ScaledGen{T}}} <: Lorentz{T}
    # the ordering of these is from left to right
    # this coincides with the convention for Julia's prod,
    # so that prod(Exp(J) for J ∈ generators) gives correct product
    generators::Gs
end

function ExpMulti(Js::NTuple{N,ScaledGen}) where {N}
    (T, Js) = _promote_eltypes(Js)
    ExpMulti{N,T}(Js)
end

ExpMulti(Js::NTuple{N,Generator}) where {N} = ExpMulti(ntuple(j -> ScaledGen(Js[j]), N))
ExpMulti(Js::Generator...) = ExpMulti(Js)

# presumably no user should ever want to do this, but it's useful internally
ExpMulti(Λ::Exp) = ExpMulti(Λ.generator)

params(Λ::ExpMulti{N}) where {N} = ntuple(j -> param(Λ.generators[j]), N)

# note the ordering; we do this because of Julia's convention for prod
_prod(::Type{<:Gen}, ::Type{<:Gen}, Λ₂::Exp, Λ₁::Exp) = ExpMulti((Λ₁.generator, Λ₂.generator))

Base.:(*)(Λ₂::Exp, Λ₁::Exp) = _prod(gentype(Λ₂), gentype(Λ₁), Λ₁, Λ₂)

Base.inv(Λ::ExpMulti{N}) where {N} = ExpMulti(ntuple(j -> inv(Exp(Λ.generators[N-j+1])).generator, N)...)

_middle_gen_sum(::Type{G}, ::Type{G}, J₂::ScaledGen, J₁::ScaledGen) where {G} = (J₂ + J₁,)
_middle_gen_sum(::Type, ::Type, J₂::ScaledGen, J₁::ScaledGen) = (J₂, J₁)
_middle_gen_sum(J₂::ScaledGen, J₁::ScaledGen) = _middle_gen_sum(gentype(J₂), gentype(J₁), J₂, J₁)

function Base.:(*)(Λ₂::ExpMulti{N2}, Λ₁::ExpMulti{N1}) where {N2,N1}
    gens2 = ntuple(j -> Λ₂.generators[j], N2-1)
    midgen = _middle_gen_sum(Λ₂.generators[end], Λ₁.generators[1])
    gens1 = ntuple(j -> Λ₁.generators[j+1], N1-1)
    ExpMulti(tuple(gens2..., midgen..., gens1...))
end 

Base.:(*)(Λ₂::Exp, Λ₁::ExpMulti) = ExpMulti(Λ₂)*Λ₁
Base.:(*)(Λ₂::ExpMulti, Λ₁::Exp) = Λ₂*ExpMulti(Λ₁)

# for testing
_slow_getindex(Λ::ExpMulti, k::Int) = prod(SMatrix(Exp(J)) for J ∈ Λ.generators)[k]

expfromgens(Js::NTuple{1,Generator}) = Exp(Js[1])
expfromgens(Js::NTuple{N,Generator}) where {N} = ExpMulti(Js)

"""
    factor(Λ::ExpMulti, Val(N))
    factor(Λ::Exp, Val(N))

Factor the exponential product into factors with at most `N` [`Exp`](@ref) factors, from right to left.
This is used to compute matrix products as efficiently as possible given that specialized
methods are only generated for products up to a certain number of `Exp` factors, currently `3`.

Note that, due to recursion, this loses type stability for long products.
"""
factor(Λ::Exp, ::Val) = (Λ,)
function factor(Λ::ExpMulti{M}, ::Val{N}) where {M,N}
    N ≥ M && return (Λ,)
    gens2 = ntuple(j -> Λ.generators[j], M-N)
    gens1 = ntuple(j -> Λ.generators[M-j+1], N)
    (factor(expfromgens(gens2), Val(N))..., expfromgens(gens1))
end

for j ∈ 1:N_GENERATED
    @eval StaticArrays.SMatrix(Λ::ExpMulti{$j}) = SMatrix{4,4,eltype(Λ)}(Tuple(Λ))
end
StaticArrays.SMatrix(Λ::ExpMulti, ::Val{N}) where {N} = prod(SMatrix(λ) for λ ∈ factor(Λ, Val(N)))

# fallback
StaticArrays.SMatrix(Λ::ExpMulti) = SMatrix(Λ, Val(N_GENERATED))

LorMatrix(Λ::ExpMulti, ::Val{N}) where {N} = LorMatrix(SMatrix(Λ, Val(N)))
LorMatrix(Λ::ExpMulti) = LorMatrix(SMatrix(Λ))

# comically inefficient fallback
Base.getindex(Λ::ExpMulti, k::Int) = LorMatrix(Λ)[k]

# inefficient fallback, not nearly as bad as getindex
function Base.:(*)(Λ::ExpMulti, v::StaticVector{4})
    for λ ∈ reverse(factor(Λ, Val(N_GENERATED)))
        # the compiler doesn't really like this because the type of λ is not fixed
        v = λ*v
    end
    v
end

# all of these should be way more efficient than the right-associative default
Base.:(*)(Λ₂::Exp, Λ₁::Exp, v::AbstractVector) = (Λ₂*Λ₁)*v
Base.:(*)(Λ₂::Exp, Λ₁::ExpMulti, v::AbstractVector) = (Λ₂*Λ₁)*v
Base.:(*)(Λ₂::ExpMulti, Λ₁::Exp, v::AbstractVector) = (Λ₂*Λ₁)*v
Base.:(*)(Λ₂::ExpMulti, Λ₁::ExpMulti, v::AbstractVector) = (Λ₂*Λ₁)*v

