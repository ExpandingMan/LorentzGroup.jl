#====================================================================================================
This is *NOT* package code

this is used to generate some of it
====================================================================================================#
using Symbolics, LinearAlgebra, StaticArrays, MacroTools
using SymbolicUtils.Code

using MacroTools: prewalk, postwalk

using Base: remove_linenums!


# these are analogous to what we have in the package without depending on it
struct Gen
    i::Int
    j::Int

    function Gen(i::Integer, j::Integer)
        if (i,j) ∉ ((1,2), (1,4),
                    (2,3), (2,4),
                    (3,1), (3,4),
                   )
            error("nope, that violated my handedness convention")
        end
        new(i, j)
    end
end

isboost(J::Gen) = J.j == 4
isrot(J::Gen) = !isboost(J)

allgens() = (Gen(1,2), Gen(1,4),
             Gen(2,3), Gen(2,4),
             Gen(3,1), Gen(3,4),
            )

function axisname(J::Gen)
    (i, j) = (J.i, J.j)
    if j == 4
        if i == 1
            :X
        elseif i == 2
            :Y
        elseif i == 3
            :Z
        else
            error("invalid generator")
        end
    elseif (i, j) == (2, 3)
        :X
    elseif (i, j) == (3, 1)
        :Y
    elseif (i, j) == (1, 2)
        :Z
    else
        error("invalid generator")
    end
end

name(J::Gen) = Symbol(:Gen, axisname(J))

function alias(J::Gen, T=nothing)
    o = Symbol(isboost(J) ? :B : :R, axisname(J))
    isnothing(T) || (o = :($o{$T}))
    o
end

function identmat()
    Num[1 0 0 0
        0 1 0 0
        0 0 1 0
        0 0 0 1
       ]
end

function Base.exp(ϕ::Num, J::Gen)
    (i, j) = (J.i, J.j)
    Λ = identmat()
    s = 4 ∈ (i,j) ? sinh(ϕ) : sin(ϕ)
    c = 4 ∈ (i,j) ? cosh(ϕ) : cos(ϕ)
    Λ[i,i] = c
    Λ[i,j] = -s
    Λ[j,i] = 4 ∈ (i,j) ? -s : s
    Λ[j,j] = c
    Λ
end

Base.exp(ϕs, Js::Gen...) = foldr(*, (exp(ϕ, J) for (ϕ, J) ∈ zip(ϕs, Js)))

# I don't know how to do this with just Symbolics
function _clean_symbolic_out(λ)
    if λ isa Expr && λ.head == :call
        prewalk(λ) do arg
            if arg == cos
                :cos
            elseif arg == sin
                :sin
            elseif arg == cosh
                :cosh
            elseif arg == sinh
                :sinh
            elseif arg == *
                :*
            elseif arg == +
                :+
            elseif arg == -
                :-
            else
                arg
            end
        end
    else
        λ
    end
end

function mateltoexpr(λ)
    if iszero(λ)
        :(zero(T))
    elseif isone(λ)
        :(one(T))
    else
        expr = _clean_symbolic_out(toexpr(λ))
        :(convert(T, $expr))
    end
end

function _getindex_body_inner!(expr, Λ, k::Integer)
    if k > 16
        push!(expr.args, :(throw(BoundsError(Λ,k))))
    else
        expr′ = Expr(:elseif, :(k == $k), mateltoexpr(Λ[k]))
        _getindex_body_inner!(expr′, Λ, k+1)
        push!(expr.args, expr′)
    end
end

exptype(T, J::Gen) = :(Exp{$T,$(alias(J))})
exptype(::Nothing, J::Gen) = exptype(:(<:Any), J)
exptype(J::Gen) = exptype(nothing, J)

scaledgentype(T, J::Gen) = :(ScaledGen{$T,$(alias(J))})
scaledgentype(::Nothing, J::Gen) = scaledgentype(:(<:Any), J)
scaledgentype(J::Gen) = scaledgentype(nothing, J)

function exptype(T, Js::Gen...)
    N = length(Js)
    tpl = map(J -> scaledgentype(T, J), Js)
    :(ExpMulti{$N,$T,Tuple{$(tpl...)}})
end

makeargvar(j::Integer) = Num(SymbolicUtils.Sym{Real}(Symbol(:ϕ, j)))

makeargvars(Js::Gen...) = ntuple(makeargvar, length(Js))

function make_getindex_body(Js::Gen...)
    ϕs = makeargvars(Js...)
    Λ = exp(ϕs, Js...)
    expr = Expr(:if, :(k == 1), mateltoexpr(Λ[1]))
    _getindex_body_inner!(expr, Λ, 2)
    expr
end

function unpack_param_expr(n::Integer)
    syms = [Symbol(:ϕ, j) for j ∈ 1:n]
    l = Expr(:tuple, syms...)
    :($l = params(Λ))
end

function make_getindex(Js::Gen...)
    type = exptype(:T, Js...)
    unp = unpack_param_expr(length(Js))
    body = make_getindex_body(Js...)
    quote
        function Base.getindex(Λ::$type, k::Int) where {T}
            $unp
            $body
        end
    end |> remove_linenums! |> unblock
end

_extract_ϕ_num(s::Symbol) = parse(Int, string(s)[end])

function _replace_trigs(expr)
    prewalk(expr) do ex
        if @capture(ex, trig_(ϕ_))
            ϕ isa Symbol || return ex
            n = _extract_ϕ_num(ϕ)
            if trig ∈ (:sin, :sinh, sin, sinh)
                Symbol(:s, n)
            elseif trig ∈ (:cos, :cosh, cos, cosh)
                Symbol(:c, n)
            end
        else
            ex
        end
    end
end

function make_mult_body_args(Λ::AbstractMatrix) 
    @variables v1 v2 v3 v4
    _replace_trigs.(_clean_symbolic_out.(toexpr.(Λ*[v1, v2, v3, v4])))
end

function trig_aliases_expr(Js::Gen...)
    map(enumerate(Js)) do (n, J)
        ssym = Symbol(:s, n)
        csym = Symbol(:c, n)
        lhs = :(($ssym, $csym))
        ϕsym = Symbol(:ϕ, n)
        if isboost(J)
            :($lhs = (sinh($ϕsym), cosh($ϕsym)))
        else
            :($lhs = sincos($ϕsym))
        end
    end
end

function make_mult_body(Js::Gen...)
    ϕs = makeargvars(Js...)
    Λ = exp(ϕs, Js...)
    unp = unpack_param_expr(length(Js))
    args = make_mult_body_args(Λ)
    trigal = trig_aliases_expr(Js...)
    quote
        $unp
        $(trigal...)
        (v1, v2, v3, v4) = (v[1], v[2], v[3], v[4])
        T = Base.promote_op(*, eltype(Λ), eltype(v))
        similar_type(v, T)($(args...))
    end |> remove_linenums! |> unblock
end

function make_mult(Js::Gen...)
    type = exptype(:S, Js...)
    body = make_mult_body(Js...)
    quote
        function Base.:(*)(Λ::$type, v::StaticVector{4}) where {S}
            $(body.args...)
        end
    end |> remove_linenums! |> unblock
end

expalias(J::Gen) = Symbol(isboost(J) ? :Boost : :Rot, axisname(J))

_expalias(J::Gen) = (prev="",
                     name=isboost(J) ? "Boost" : "Rot",
                     ax=string(axisname(J)),
                    )

function _combine_exp_aliases(as1, as2)
    if as1.name == as2.name
        (prev=as1.prev, name=as1.name, ax=as1.ax*as2.ax)
    else
        (prev=as1.prev*as1.name*as1.ax, name=as2.name, ax=as2.ax)
    end
end

function expalias(Js::Gen...)
    init = (prev="", name="", ax="")
    o = foldl(_combine_exp_aliases, _expalias.(Js); init)
    Symbol(o.prev*o.name*o.ax)
end

function _alias_constructors(ali, Js::Gen...)
    args = [Symbol(:ϕ, j) for j ∈ 1:length(Js)]
    gens = [:(ϕs[$j]*$(alias(Js[j])){T}()) for j ∈ 1:length(Js)]
    otype = length(Js) == 1 ? :Exp : :ExpMulti
    quote
        function $ali($((:($a::Number) for a ∈ args)...))
            ϕs = promote($((:(float($a)) for a ∈ args)...))
            T = eltype(ϕs)
            $otype($(gens...))
        end
    end
end

function make_alias(Js::Gen...; expor::Bool=length(Js) ≤ 2)
    ali = expalias(Js...)
    type = exptype(:T, Js...)
    ls = [:(const $ali{T} = $type),
         _alias_constructors(ali, Js...).args...,
         ]
    expor && push!(ls, :(export $ali))
    quote
        $(ls...)
    end |> remove_linenums! |> unblock
end

_gen_file_preamble() = """
#====================================================================================================
AUTOMATICALLY GENERATED FILE

This file was generated by codegen.jl and should not be manually modified.
====================================================================================================#
"""

function printblock(io::IO, expr::Expr)
    if expr.head == :block
        for arg ∈ expr.args
            println(io, arg)
        end
    else
        println(io, expr)
    end
    println(io)
end

function make_file(f, io::IO)
    print(io, _gen_file_preamble())
    print(io, "\n\n")
    for J ∈ allgens()
        printblock(io, f(J))
    end
    for (J1, J2) ∈ Iterators.product(allgens(), allgens())
        J1 == J2 && continue
        printblock(io, f(J1, J2))
    end
    for (J1, J2, J3) ∈ Iterators.product(allgens(), allgens(), allgens())
        J1 == J2 && continue
        J2 == J3 && continue
        printblock(io, f(J1, J2, J3))
    end
end

make_getindex_file(io::IO) = _make_file(make_getindex, io)
make_mult_file(io::IO) = _make_file(make_mult, io)

function make_file(f, fname::AbstractString; overwrite::Bool=false)
    if isfile(fname)
        overwrite || error("file already exists; use overwrite=true")
        rm(fname)
    end
    open(io -> make_file(f, io), fname, write=true, create=true)
end

function codegen(dir::AbstractString=joinpath(@__DIR__,"gen"); overwrite::Bool=false)
    if isdir(dir)
        overwrite || error("directory already exists; use overwrite=true")
        rm(dir, recursive=true, force=true)
    end
    mkdir(dir)
    cd(dir) do
        make_file(make_getindex, "exp_getindex.jl")
        make_file(make_mult, "exp_mult.jl")
        make_file(make_alias, "exp_alias.jl")
    end
end
