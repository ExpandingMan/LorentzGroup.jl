import PrecompileTools

PrecompileTools.@compile_workload begin
    RX() + RX()
    RX() + RY()
    RZ() + BY()

    exp(0.2*RX())
    exp(0.2*RY())
    exp(0.2*RZ())
    exp(rapidity(0.5)*BX())
    exp(rapidity(0.5)*BY())
    exp(rapidity(0.5)*BZ())

    RotX(0.2)*RotY(0.2)
    RotX(0.2)*BoostY(0.2)

    v = SA{Float64}[0,0,0,1]

    RotX(0.2)*v
    RotY(0.2)*v
    RotZ(0.2)*v
    BoostX(0.0)*v
    BoostY(0.0)*v
    BoostZ(0.0)*v

    allgens = (RX, RY, RZ, BX, BY, BZ)

    for (J1, J2) ∈ Iterators.product(allgens, allgens)
        exp(0.5*J1())*exp(0.5*J2())
        exp(0.5*J1())*exp(0.5*J2())*v
    end
    for (J1, J2, J3) ∈ Iterators.product(allgens, allgens, allgens)
        exp(0.5*J1())*exp(0.5*J2())*exp(0.5*J3())
        exp(0.5*J1())*exp(0.5*J2())*exp(0.5*J3())*v
    end
end
