# LorentzGroup

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/LorentzGroup.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/LorentzGroup.jl/main?style=for-the-badge)](https://gitlab.com/ExpandingMan/LorentzGroup.jl/-/pipelines)

Julia package for applying and manipulating matrix representations of the [Lorentz
group](https://en.wikipedia.org/wiki/Lorentz_group) $SO(3,1)$.

Originally this package was intended to be as close as possible to the popular
[Rotations.jl](https://github.com/JuliaGeometry/Rotations.jl).  While similar in usage, this package
is more grounded in the theory of Lie groups, in particular being based on the exponentiation of the
generators of the Lorentz group, which in LorentzGroup.jl are represented by the types `Gen` and
`ScaledGen`.

## Example
```julia
julia> using LorentzGroup;

julia> v = @SVector Float64[1,0,0,1];

julia> exp(0.2*RY())*v  # rotation about Y axis
4-element SVector{4, Float64} with indices SOneTo(4):
  0.9800665778412416
  0.0
 -0.19866933079506122
  1.0

julia> RotY(0.2)*v;  # equivalent to above

julia> exp(rapidity(0.9)*BX())*v  # boost along X axis
4-element SVector{4, Float64} with indices SOneTo(4):
 0.22941573387056202
 0.0
 0.0
 0.22941573387056202

julia> BoostX(rapidity(0.9))*v;  # equivalent to above

julia> exp(π*RZ())*exp(rapidity(0.5)*BY())*v  # boost along Y followed by rotation about Z
4-element SVector{4, Float64} with indices SOneTo(4):
 -0.9999999999999999
  0.5773502691896262
  0.0
  1.1547005383792517

julia> w = RotZBoostY(π, rapidity(0.5))*v;  # equivalent to above

julia> minkowski(w) ≈ minkowski(v)  # preserves ηwv to within numeric approximation
true
```
See the [documentation](https://ExpandingMan.gitlab.io/LorentzGroup.jl/).


## Conventions
The metric signature is $(+,+,+,-)$, with time on index 4.  This convention is chosen to retain as
much compatibility as possible with both Julia's default 1-based indexing and any existing
functionality for purely spatial tensors.

## How this was made
We automatically generate code similar to that which appears in Rotations.jl by converting matrix
products created with [Symbolics.jl](https://symbolics.juliasymbolics.org/stable/) into Julia code.
We do not, however, use the built in `build_function`, but instead do some meta-programming to
generate highly efficient expressions.  The symbolic stuff is used to generate source files in the
repository, therefore no symblic stuff is going on at run time and Symbolics.jl is not a dependency.

