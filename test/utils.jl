using LorentzGroup, StaticArrays, LinearAlgebra, Random
using Test


# this has no guarantees other than the character
function randvector(char)
    v = randn(SVector{3})
    s = rand((-1, 1))
    if char == 0
        v4 = s*norm(v)
    elseif char > 0  # spacelike
        v4 = s*rand()*norm(v)
    elseif char < 0  # timelike
        v4 = s*norm(v)
        v = rand()*v
    end
    vcat(v, v4)
end
randnull() = randvector(0)
randspacelike() = randvector(1)
randtimelike() = randvector(-1)

_groupeltypes() = (RotX, RotY, RotZ, BoostX, BoostY, BoostZ)

function invartests1(char, N::Integer)
    v = randvector(char)
    v2 = minkowski(v)
    for k ∈ 1:N
        for T ∈ _groupeltypes()
            @test minkowski(rand(T)*v) ≈ v2 atol=10^(-3)
        end
    end
end

function invartests2(char, N::Integer)
    v = randvector(char)
    v2 = minkowski(v)
    for k ∈ 1:N
        for (T1, T2) ∈ Iterators.product(_groupeltypes(), _groupeltypes())
            @test minkowski(rand(T1)*rand(T2)*v) ≈ v2 atol=10^(-3)
        end
    end
end

function invartests3(char, N::Integer)
    v = randvector(char)
    v2 = minkowski(v)
    for k ∈ 1:N
        for (T1, T2, T3) ∈ Iterators.product(_groupeltypes(), _groupeltypes(), _groupeltypes())
            @test minkowski(rand(T1)*rand(T2)*rand(T3)*v) ≈ v2 atol=10^(-3)
        end
    end
end
