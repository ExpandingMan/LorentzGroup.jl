using LorentzGroup, StaticArrays, LinearAlgebra, Random
using Test

using LorentzGroup: ScaledGen, Exp, ExpMulti, gentype

Random.seed!(999)

include("utils.jl")

@testset "vector stuff" begin
    @test lorentzfactor(0.0) == 1.0
    @test lorentzfactor(1.0) == Inf
    @test lorentzfactor(0.1) ≈ 1.005 atol=10^(-3)
    @test lorentzfactor(0.2) ≈ 1.021 atol=10^(-3)
    @test lorentzfactor(0.5) ≈ 1.155 atol=10^(-3)
    @test lorentzfactor(1.55f0) ≈ 0.8444f0 atol=10^(-3)
    @test lorentzfactor(0.9) ≈ 2.294 atol=10^(-3)
    @test lorentzfactor(2.0) ≈ 0.5774 atol=10^(-3)

    @test rapidity(0.0) == 0
    @test rapidity(0) == 0
    @test rapidity(1.0) == Inf
    @test rapidity(0.5) ≈ 0.5493 atol=10^(-4)

    @test minkowski(SA[1,0,0,0], SA[1,0,0,0]) == 1.0
    @test minkowski([1,0,0,0], [1,0,0,0]) == 1.0
    @test minkowski(SA[1,0,0,0], SA[0,1,0,0]) == 0.0
    @test minkowski(SA[1,0,0,1]) == 0.0

    for j ∈ 1:10
        @test minkowski(randnull()) ≈ 0.0 atol=10^(-5)
        @test minkowski(randspacelike()) > 0.0
        @test minkowski(randtimelike()) < 0.0
        @test minkowski(minkowskinormalize(randspacelike())) ≈ 1.0 atol=10^(-6)
        @test minkowski(minkowskinormalize(randtimelike())) ≈  -1.0 atol=10^(-6)
    end
end

@testset "generators" begin
    @testset "basis" begin
        @test RX() == [0 0 0 0
                       0 0 -1 0
                       0 1 0 0
                       0 0 0 0]
        @test RY() == [0 0 1 0
                       0 0 0 0
                       -1 0 0 0
                       0 0 0 0]
        @test RZ() == [0 -1 0 0
                       1 0 0 0
                       0 0 0 0
                       0 0 0 0]
        @test BX() == [0 0 0 -1
                       0 0 0 0
                       0 0 0 0
                       -1 0 0 0]
        @test BY() == [0 0 0 0
                       0 0 0 -1
                       0 0 0 0
                       0 -1 0 0]
        @test BZ() == [0 0 0 0
                       0 0 0 0
                       0 0 0 -1
                       0 0 -1 0]
        @test GenMatrix(RX()) == RX()
        @test GenMatrix(RX()) + GenMatrix(RY()) == [0 0 1 0
                                                    0 0 -1 0
                                                    -1 1 0 0
                                                    0 0 0 0]
        @test GenMatrix(BZ()) - GenMatrix(RZ()) == [0 1 0 0
                                                    -1 0 0 0
                                                    0 0 0 -1
                                                    0 0 -1 0]
        @test -GenMatrix(BY()) == [0 0 0 0
                                   0 0 0 1
                                   0 0 0 0
                                   0 1 0 0]
        @test typeof(RZ() + RY()) <: GenMatrix
        @test typeof(BX() + RZ()) <: GenMatrix
        @test typeof(BX() - RY()) <: GenMatrix
        @test gentype(BY()) == BY
        @test gentype(RZ{Int}()) == RZ
        @test LorentzGroup.param(RX()) == 1
        @test LorentzGroup.param(BZ()) == 1
    end

    @testset "scaled" begin
        J = ScaledGen(RX, 0.5)
        K = ScaledGen(BY, 0.5)
        @test typeof(J) == ScaledGen{Float64,RX}
        @test typeof(K) == ScaledGen{Float64,BY}
        @test J ≡ 0.5*RX()
        @test K ≡ 0.5*BY()
        @test J + J == 1.0*RX()
        @test K - K == 0.0*BY()
        @test typeof(J + K) <: GenMatrix
        @test J + K == [0 0 0 0
                        0 0 -0.5 -0.5
                        0 0.5 0 0
                        0 -0.5 0 0]
        @test typeof(K - J) <: GenMatrix
        @test K - J == [0 0 0 0
                        0 0 0.5 -0.5
                        0 -0.5 0 0
                        0 -0.5 0 0]
        @test typeof(2*J) == typeof(J)
        @test typeof(2*K) == typeof(K)
        @test 2*J == [0 0 0 0
                      0 0 -1 0
                      0 1 0 0
                      0 0 0 0]
        @test RX() + RX() == 4*J
        @test BY() + BY() == 4*K
        @test typeof(RX() + J) == typeof(J)
        @test typeof(K - BY()) == typeof(K)
        @test typeof(J*K) <: StaticMatrix
        @test !(typeof(J*K) <: LorentzGroup.Generator)

        @inferred RX() + BX()
        @inferred rapidity(0.2)*BZ()
        @inferred RZ() - BY()
        @inferred RY() + 0.1*RY()
    end
end

@testset "exp" begin
    @testset "single" begin
        @test typeof(Exp(BX())) == Exp{Float64,BX}
        @test typeof(Exp(1.0*BX())) == Exp{Float64,BX}
        @test typeof(Exp(BX{Float32}())) == Exp{Float32,BX}
        @test exp(RY()) == Exp(RY())
        @test exp((π/2)*RY()) == Exp{Float64,RY}(π/2)
        @test exp((π/3)*RZ()) == RotZ(π/3)
        @test typeof(exp(RZ{Float32}())) == RotZ{Float32}
        @test typeof(exp(Float32(π/3)*RZ{Float32}())) == RotZ{Float32}
        @test exp(Float32(π/3)*RZ{Float32}()) == RotZ{Float32}(π/3)
        @test typeof(BoostZ{Float32}(π/3)) == BoostZ{Float32}
        @test inv(exp(RY())) == exp(-RY())
        @test inv(exp(0.3*BZ())) == exp(-0.3*BZ())
        @test one(RotZ{Float64}) == exp(0.0*RZ())
        @test gentype(RotZ(0.0)) == RZ
        @test gentype(BoostY(0.5)) == BY
        @test RotX(π/2) ≈ Float64[1 0 0 0
                                  0 0 -1 0
                                  0 1 0 0
                                  0 0 0 1]
        @test BoostZ(1/2) ≈ Float64[1 0 0 0
                             0 1 0 0
                             0 0 1.12763 -0.521095
                             0 0 -0.521095 1.12763] atol=10^(-5)
        @test RotY(-π/2) ≈ Float64[0 0 -1 0
                                   0 1 0 0
                                   1 0 0 0
                                   0 0 0 1]
        @test typeof(RotY(π/2)*RotY(π/2)) == RotY{Float64}
        @test RotY(π/2)*RotY(π/2) ≈ Float64[-1 0 0 0
                                            0 1 0 0
                                            0 0 -1 0
                                            0 0 0 1]
        @test typeof(LorMatrix(RotX(π/2))) == LorMatrix{Float64}
        @test LorMatrix(RotX(π/2)) ≈ Float64[1 0 0 0
                                             0 0 -1 0
                                             0 1 0 0
                                             0 0 0 1]
        @inferred RotX(0.2)
        @inferred BoostZ(0.3)
        @inferred RotX(π/2)*RotX(π/2)
        @inferred BoostY(rapidity(0.5))*BoostY(rapidity(0.2))
        @inferred exp((π/6)*RZ())
        @inferred exp(rapidity(0.5)*BX())

        for char ∈ (-1, 0, 1)
            invartests1(char, 5)
        end
    end

    @testset "multi" begin
        @test typeof(ExpMulti(0.5*BX(), 0.5*RY())) == ExpMulti{2,Float64,
                                                               Tuple{ScaledGen{Float64,BX},ScaledGen{Float64,RY}},
                                                              }
        @test typeof(exp(0.5*BY())*exp(0.5*RZ())) == ExpMulti{2,Float64,
                                                              Tuple{ScaledGen{Float64,BY},ScaledGen{Float64,RZ}},
                                                             }

        # yes, these should be *exact*
        @test ExpMulti(0.5*RZ(), 0.5*BX()) == exp(0.5*RZ())*exp(0.5*BX())
        @test RotXY(π/2,-π/2) == exp((π/2)*RX())*exp(-(π/2)*RY())
        @test inv(RotXBoostY(π/3, 1.0)) == BoostYRotX(-1.0, -π/3)

        @test RotXY(π/2,-π/2) ≈ Float64[0 0 -1 0
                                        -1 0 0 0
                                        0 1 0 0
                                        0 0 0 1] atol=10^(-5)
        @test LorMatrix(RotXY(π/2,-π/2)) ≈ Float64[0 0 -1 0
                                                   -1 0 0 0
                                                   0 1 0 0
                                                   0 0 0 1] atol=10^(-5)

        @test RotXBoostX(π, rapidity(0.2)) ≈ Float64[1.02062 0 0 -0.204124
                                                     0 -1 0 0
                                                     0 0 -1 0
                                                     -0.204124 0 0 1.02062] atol=10^(-5)
        @test BoostYZ(rapidity(0.2), rapidity(0.3)) ≈ Float64[1 0 0 0
                                                              0 1.02062 0.0641941 -0.21398
                                                              0 0 1.04828 -0.314485
                                                              0 -0.204124 -0.32097 1.0699] atol=10^(-5)

        @inferred exp(0.5*RX())*exp(0.5*BY())
        @inferred rand(BoostX)*rand(BoostY)*rand(RotZ)

        v = SA{Float64}[0,1,0,1]
        @inferred rand(RotX)*rand(RotY)*v
        @inferred rand(RotX)*rand(BoostZ)*rand(RotZ)*v

        for char ∈ (-1, 0, 1)
            invartests2(char, 5)
            invartests3(char, 2)
        end
    end
end

@testset "extras" begin
    @testset "identity" begin
        I = LorIdentity{Float32}()
        @test eltype(I) == Float32
        
        I = LorIdentity()
        @test eltype(I) == Float64
        @test one(Lorentz) ≡ LorIdentity()
        @test I == [1 0 0 0
                    0 1 0 0
                    0 0 1 0
                    0 0 0 1]
        for c ∈ (-1, 0, 1), j ∈ 1:3
            v = randvector(c)
            @test I*v == v
        end
        for j ∈ 1:10
            R = rand(RotX)
            @test I*R ≡ R
            B = rand(BoostY)
            @test B*I ≡ B
        end
    end

    @testset "fourvelfromcoordvel" begin
        @test fourvelfromcoordvel(Float64[0,0,0]) ≈ Float64[0,0,0,1]
        @test fourvelfromcoordvel(Float64[0.1,0,0]) ≈ Float64[0.1005, 0, 0, 1.0050] atol=10^(-4)
        @test fourvelfromcoordvel(Float64[0.1,0.2,0.2]) ≈ Float64[0.105,0.210,0.210,1.048] atol=10^(-3)
        @test fourvelfromcoordvel(Float64[0,2,0]) ≈ Float64[0,1.1547,0,0.5773] atol=10^(-4)
    end

    @testset "comoving" begin
        @test comoving(Float64[0,0,0,1]) isa LorMatrix{Float64}
        @test comoving(Float64[0,0,0,1]) == one(SMatrix{4,4,Float64})
        @test comoving(Float64[0,0,0,1]) == one(LorMatrix{Float64})
        @test comoving(Float64[0,0,0,1], normalize=false) == one(LorMatrix{Float64})
        for j ∈ 1:10
            v = minkowskinormalize(randtimelike())
            Λ = comoving(v)
            @test Λ*v ≈ Float64[0,0,0,1] atol=10^(-5)
        end
    end
end
