```@meta
CurrentModule = LorentzGroup
```

# API

```@index
```

## Generators
```@docs
Generator
GenMatrix
Gen
ScaledGen
gentype
```

## Vector Representation
```@docs
Lorentz
LorMatrix
Exp
ExpMulti
comoving
```

## Utilities
```@docs
LorIdentity
minkowski
minkowskinormalize
rapidity
lorentzfactor
fourvelfromcoordvel
```

## Internals
```@docs
factor
N_GENERATED
```
