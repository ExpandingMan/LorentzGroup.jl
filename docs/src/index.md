```@raw html
---
layout: home

hero:
    name: LorentzGroup.jl Documentation
    tagline: SO(3,1) in Julia
    actions:
        - theme: alt
          text: Getting Started
          link: /usage
        - theme: alt
          text: API Docs
          link: /api
---
```
