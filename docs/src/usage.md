```@meta
CurrentModule = LorentzGroup
```

# [LorentzGroup](https://gitlab.com/ExpandingMan/LorentzGroup.jl)

This package implements the 4-dimensional vector representation of the Lorentz group
``SO^{+}(3,1)`` as [static arrays](https://github.com/JuliaArrays/StaticArrays.jl).  Usage of this
package is similar to [Rotations.jl](https://github.com/JuliaGeometry/Rotations.jl).  In order to
conform to common programming conventions as best as possible, in particular Julia's 1-based
indexing and existing packages for ``SO(3)`` vectors, this package adopts the metric signature
``(+,+,+,-)``.

## Usage Overview
```julia
julia> using LorentzGroup;

julia> v = @SVector Float64[1,0,0,1];

julia> exp(0.2*RY())*v  # rotation about Y axis
4-element SVector{4, Float64} with indices SOneTo(4):
  0.9800665778412416
  0.0
 -0.19866933079506122
  1.0

julia> RotY(0.2)*v;  # equivalent to above

julia> exp(rapidity(0.9)*BX())*v  # boost along X axis
4-element SVector{4, Float64} with indices SOneTo(4):
 0.22941573387056202
 0.0
 0.0
 0.22941573387056202

julia> BoostX(rapidity(0.9))*v;  # equivalent to above

julia> exp(π*RZ())*exp(rapidity(0.5)*BY())*v  # boost along Y followed by rotation about Z
4-element SVector{4, Float64} with indices SOneTo(4):
 -0.9999999999999999
  0.5773502691896262
  0.0
  1.1547005383792517

julia> w = RotZBoostY(π, rapidity(0.5))*v;  # equivalent to above

julia> minkowski(w) ≈ minkowski(v)  # preserves ηwv to within numeric approximation
true
```

### Highlights
- The generators of the group are given by `RX, RY, RZ, BX, BY, BZ` where `R` is for rotation and
    `B` is for boost.
- The boost parameter is always rapidity for sake of consistency, use `rapidity` to convert from
    coordinate speed.
- Direct exponentiation of these generators (i.e. not linear combinations thereof) are efficient,
    e.g. `exp(0.3*RZ())` should compile to very nearly the same code as `RotZ(0.3)`.
- Exponentials of the generators are group elements of type `Exp`, products of these are of type
    `ExpMulti`.  Operations between these are very efficient.
- `Exp` and products of two of `Exp` have familiar aliases, e.g. `RotX, RotXY, RotXBoostY, BoostYZ`
    et cetera.
- `GenMatrix` and `LorMatrix` represent materialized `SMatrix` objects for the generators and group
    elements respectively.  For example, the object returned by `LorMatrix(RotXY(ϕ1)*BoostYZ(ϕ2))`
    is an explicit `StaticMatrix` of abstract type `Lorentz`.  Indexing and multiplication of such
    objects are efficient, but matrix multiplications among them are not.
- Efficient methods for indexing and multiplication of vectors are provided for produts of up to 3
    `Exp`.  Larger products of group elements will automatically decompose, but in some cases it
    might be desirable to explicitly materialize to a matrix using `LorMatrix`.
- We adopt a sign convention for the generators such that the exponential of the generator is always
    the familiar transformation.  For example `exp(RX())`, `exp(RY())`, `exp(BY())` are the
    *counter-clockwise* rotation about `X`, *counter-clockwise* rotation about `Y` and *forward*
    boost along `Y`, respectively.


## How this package works
This package is designed around exponentials of the generators of the ``\mathfrak{so}(3,1)``
algebra.  [Symbolics.jl](https://symbolics.juliasymbolics.org/stable/) was used to generate code for
efficient indexing and multiplication of certain group elements.  In particular, the group elements
for which efficient methods exist are products of up to 3 exponentials of the generators in the
provided basis.  This means that, for example, efficient methods exist for
`Λ = RotXY(π/2,π/3)*BoostZ(1.0)` so that e.g. multiplying a vector by this object is highly efficient,
however `exp((π/2)*RX() + (π/3)*RY() + BZ())` retursn a materialzied `LorMatrix` object, operations
between which are less efficient, and `Λ^5` lacks the highly efficient `Base.getindex` and
multiplication methods that `Λ` enjoys.  Because of this, it may be desirable to explicitly convert
group elements that are "far away" from the efficient ones with `LorMatrix`.
