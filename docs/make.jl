using LorentzGroup
using Documenter, DocumenterVitepress

makedocs(;
    modules=[LorentzGroup],
    repo=Remotes.GitLab("ExpandingMan", "LorentzGroup.jl"),
    sitename="LorentzGroup.jl",
    format = DocumenterVitepress.MarkdownVitepress(
         repo="https://gitlab.com/ExpandingMan/LorentzGroup.jl",
    ),
    pages=[
        "LorentzGroup.jl" => "index.md",
        "Usage" => "usage.md",
        "API" => "api.md",
    ],
)
